{
    services.jackett = {
        enable = true;
        openFirewall = true;
    };
    services.radarr = {
        enable = true;
        openFirewall = true;
    };
    services.sonarr = {
        enable = true;
        openFirewall = true;
    };
    services.bazarr = {
        enable = true;
        openFirewall = true;
    };
    services.lidarr = {
        enable = true;
        openFirewall = true;
    };
    services.prowlarr = {
        enable = true;
        openFirewall = true;
    };
    services.aria2 = {
        enable = true;
        openPorts = true;
        extraArguments = "--rpc-listen-all";
    };
    services.mullvad-vpn = {
        enable = true;
        enableExcludeWrapper = false;
    };
    services.transmission = {
        enable = true;
        openFirewall = true;
        openRPCPort = true;
    };
}