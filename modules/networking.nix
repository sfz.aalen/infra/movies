{
  networking = {
    hostName = "movies";
    networkmanager.enable = true;
    firewall.enable = true;

    useDHCP = false;
    defaultGateway = "10.20.42.2";
    nameservers = [ "10.20.42.2" ];

    interfaces.ens18 = {
      useDHCP = false;
      ipv4.addresses = [
        { address = "10.20.42.157"; prefixLength = 24; }
      ];
    };
  };
}
