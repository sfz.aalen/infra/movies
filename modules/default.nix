{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    vim
    jq
  ];

  imports = [
    ./boot.nix
    ./networking.nix
    ./nix.nix
    ./shell.nix
    ./ssh.nix
    ./user.nix
    ./localisation.nix
    ./mov.nix
  ];

  system.stateVersion = "23.05";
}

