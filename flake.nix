{
  description = "Movie VM";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, treefmt-nix }:
    let
      lib = nixpkgs.lib;
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
      };
      treefmtEval = treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
    in
    {
      formatter."${system}" = treefmtEval.config.build.wrapper;
      checks."${system}".formatter = treefmtEval.config.build.check self;
      packages."${system}".install = import ./install.nix { inherit pkgs; };
      nixosConfigurations = {
        movies = lib.nixosSystem {
          inherit system;
          modules = [
            ./modules
            ./hardware-configuration.nix
          ];
        };
      };
    };
}
